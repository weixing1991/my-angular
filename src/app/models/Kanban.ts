import { Item } from './Item';

export interface Kanban{
    title:String,
    items:Item[]
}