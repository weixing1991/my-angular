import { Component, OnInit } from '@angular/core';
import { Kanban } from '../ models/Kanban';

@Component({
  selector: 'app-kanban-board',
  templateUrl: './kanban-board.component.html',
  styleUrls: ['./kanban-board.component.scss']
})
export class KanbanBoardComponent implements OnInit {
  kanbans:Kanban[];
  constructor() { }

  ngOnInit(): void {
    this.kanbans = [{title:"TO DO",items:[{description:"Create Project"},]},{title:"TO DO",items:[{description:"Create Project"},]}]
  }

}
