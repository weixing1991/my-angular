import { Component, OnInit } from '@angular/core';
import { faCoffee,faHome } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Angular';
  faCoffee=faCoffee;
  faHome=faHome;


  constructor(){}

  ngOnInit(){
    
  }
}
